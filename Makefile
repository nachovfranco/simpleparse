CC = gcc
CFLAGS = -O2
ALL_FLAGS = -Wall

all: simpleparse.o

simpleparse.o: simpleparse.c
	$(CC) $(CFLAGS) -c simpleparse.c
	
clean:
	rm -f simpleparse.o

.PHONY : clean
