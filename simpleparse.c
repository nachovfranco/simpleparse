/*
 * This small library is free software and is licensed under the 
 * GNU General Public License v3
 * 
 * Author: Ignacio Vidal (nacho.vfranco@gmail.com)
 * Date: February 2016
 * 
 */

#include <string.h>
#include <stdlib.h>

//If string is present when calling the program, returns a pointer
//to the following string.
//Else, return a NULL pointer
char* parseSingleArg(int argc, char **argv, char *string)
{
	char *argvalue=NULL;
	int i;
	int pos=0;
	for(i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],string))
		{
			pos=i;
			argvalue = argv[pos+1];
		}
	}	
	return argvalue;
}

int parseBinaryOption(int argc, char **argv, char *string)
{
	char *argvalue=NULL;
	int i;
	int optionIsPresent=0;
	for(i=1;i<argc;i++)
	{
		if(!strcmp(argv[i],string))
			optionIsPresent=1;
	}	
	//return 1 if the option is present in the command line.
	//Else, return 0
	return optionIsPresent;
}	
