# Simpleparse

**Simpleparse** is a simple parsing library to easily integrate command line arguments into your C programs. It is ideal if you have a pet project or if
you do not want anything too complicated. Just import the header, compile it and use it straight in your C code. That's all.
